import React from 'react';
import {useState,useEffect} from 'react';
import {Link} from 'react-router-dom';
function Shop() {
    useEffect(()=>{
        fetchItems()

    },[])
    
    const[items,setItems]=useState([]);
    const fetchItems=async()=>{
        const data=await fetch('https://5f3eac0813a9640016a68fea.mockapi.io/api/v1/books')

        const items=await data.json();
        console.log(items);
        setItems(items);
    }
   
  return (
    <div className="App">
      {items.map(item=>(
    <h1 key={item.id}>
        {/* <Link to={{pathname:`/shop/${item.id}`,state:{description:item.definition}}}> {item.term}</Link> */}
                {item.name}
                <img src={item.avatar}></img>
          </h1>
      ))}
      </div>
   
  ) 
  
}

export default Shop;
